import {apiVeganRecipeInfo, apiVeganRecipes, Ingredient, Recipe, RecipeWithIngredients } from "../interfaces";

export const getRecipesfromApiRecipes = (apiRecipes: apiVeganRecipes[]) => {
  let respRecipes: Recipe[] = [];
  respRecipes = apiRecipes.map((recipe) => {
    const recipeResp: Recipe = {
      id: recipe.id,
      title: recipe.title,
      image: recipe.image,
    };
    return recipeResp;
  });
  return respRecipes;
};

export const getRecipesWithIngredientsfromApiRecipesInfo = (apiRecipe: apiVeganRecipeInfo) => {
    let respRecipes: RecipeWithIngredients;
    let ingredients: Ingredient[];
    ingredients= apiRecipe.extendedIngredients.map((ingredient) => {
        const recipeResp: Ingredient = {
          id: ingredient.id,
          name: ingredient.name
        };
        return recipeResp;
      });
    respRecipes =  {
        id: apiRecipe.id,
        title: apiRecipe.title,
        image: apiRecipe.image,
        ingredients 
    };
    return respRecipes;
  };

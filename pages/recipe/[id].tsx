import { GetStaticProps, NextPage, GetStaticPaths } from "next";
import { Badge, Card, Grid, Text } from "@nextui-org/react";

import {
  apiVeganRecipeInfo,
  apiVeganRecipes,
  apiVeganRecipesTopLevel,
  RecipeWithIngredients,
} from "../../interfaces";
import { recipesApi, getRecipeInfoById } from "../../api";
import { NavbarUI } from "../../components/ui";
import { getRecipesWithIngredientsfromApiRecipesInfo } from "../../utils";
import { RecipeLayout } from "../../components/layouts";

interface Props {
  recipeInfo: RecipeWithIngredients;
}

const RecipeById: NextPage<Props> = ({ recipeInfo }) => (
  <RecipeLayout recipe={recipeInfo}>
    <Text h2 css={{ padding: "30px" }}>
      {recipeInfo.title}
    </Text>
    <Grid.Container css={{ marginTop: "5px" }} gap={2}>
      <Grid xs={12} sm={4}>
        <Card isHoverable css={{ padding: "30px" }}>
          <Card.Body>
            <Card.Image
              src={recipeInfo.image}
              alt={recipeInfo.title}
              width="100%"
              height={200}
            />
          </Card.Body>
        </Card>
      </Grid>
      <Grid xs={12} sm={8} direction={"column"}>
        <Text h3>Ingredientes</Text>
        <Grid.Container gap={2}>
          {recipeInfo.ingredients.map((ingredient) => (
            <Grid key={ingredient.id}>
              <Badge color="success" variant="flat" isSquared>
                {ingredient.name}
              </Badge>
            </Grid>
          ))}
        </Grid.Container>
      </Grid>
    </Grid.Container>
  </RecipeLayout>
);

export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await recipesApi.get<apiVeganRecipesTopLevel>(
    "/recipes/complexSearch?diet=vegetarian&sortDirection=asc"
  );
  const recipes: apiVeganRecipes[] = data.results;
  return {
    paths: recipes.map(({ id }) => ({
      params: { id: id.toString() },
    })),
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { id } = params as { id: string };
  const apiRecipeInfo: apiVeganRecipeInfo = await getRecipeInfoById(id);
  const recipeInfo: RecipeWithIngredients =
    getRecipesWithIngredientsfromApiRecipesInfo(apiRecipeInfo);

  return { props: { recipeInfo } };
};

export default RecipeById;

import { NextPage, GetStaticProps } from "next";
import { Grid } from "@nextui-org/react";

import { recipesApi } from "../api";
import { MainLayout } from "../components/layouts";
import { RecipeCard } from "../components/ui";

import { Recipe } from "../interfaces";
import { apiVeganRecipesTopLevel } from "../interfaces/apiVeganRecipes";
import { getRecipesfromApiRecipes } from "../utils";

interface Props {
  recipes: Recipe[];
}

const Home: NextPage<Props> = ({ recipes }) => (
  <MainLayout title="Kttita_blog - Home">
    <Grid.Container gap={2} justify="flex-start">
      {recipes.map((recipe: Recipe) => (
        <RecipeCard key={recipe.id} recipe={recipe} />
      ))}
    </Grid.Container>
  </MainLayout>
);

export const getStaticProps: GetStaticProps = async (ctx) => {
  const { data } = await recipesApi.get<apiVeganRecipesTopLevel>(
    "/recipes/complexSearch?diet=vegetarian&sortDirection=asc"
  );
  const recipes: Recipe[] = getRecipesfromApiRecipes(data.results);
  return { props: { recipes } };
};

export default Home;

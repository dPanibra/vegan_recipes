export interface apiVeganRecipesTopLevel {
    results:      apiVeganRecipes[];
    offset:       number;
    number:       number;
    totalResults: number;
}

export interface apiVeganRecipes {
    id:        number;
    title:     string;
    image:     string;
    imageType: string;
}

export interface apiVeganRecipeInfo {
    vegetarian:               boolean;
    vegan:                    boolean;
    glutenFree:               boolean;
    dairyFree:                boolean;
    veryHealthy:              boolean;
    cheap:                    boolean;
    veryPopular:              boolean;
    sustainable:              boolean;
    lowFodmap:                boolean;
    weightWatcherSmartPoints: number;
    gaps:                     string;
    preparationMinutes:       number;
    cookingMinutes:           number;
    aggregateLikes:           number;
    healthScore:              number;
    creditsText:              string;
    license:                  string;
    sourceName:               string;
    pricePerServing:          number;
    extendedIngredients:      ExtendedIngredient[];
    id:                       number;
    title:                    string;
    readyInMinutes:           number;
    servings:                 number;
    sourceUrl:                string;
    image:                    string;
    imageType:                string;
    summary:                  string;
    cuisines:                 string[];
    dishTypes:                any[];
    diets:                    string[];
    occasions:                any[];
    winePairing:              WinePairing;
    instructions:             string;
    analyzedInstructions:     AnalyzedInstruction[];
    originalId:               null;
    spoonacularSourceUrl:     string;
}

interface AnalyzedInstruction {
    name:  string;
    steps: Step[];
}

interface Step {
    number:      number;
    step:        string;
    ingredients: Ent[];
    equipment:   Ent[];
    length?:     Length;
}

interface Ent {
    id:            number;
    name:          string;
    localizedName: string;
    image:         string;
    temperature?:  Length;
}

interface Length {
    number: number;
    unit:   string;
}
interface ExtendedIngredient {
    id:           number;
    aisle:        string;
    image:        string;
    consistency:  string;
    name:         string;
    nameClean:    string;
    original:     string;
    originalName: string;
    amount:       number;
    unit:         string;
    meta:         string[];
    measures:     Measures;
}

interface Measures {
    us:     Metric;
    metric: Metric;
}

interface Metric {
    amount:    number;
    unitShort: string;
    unitLong:  string;
}

interface WinePairing {
    pairedWines:    string[];
    pairingText:    string;
    productMatches: ProductMatch[];
}

interface ProductMatch {
    id:            number;
    title:         string;
    description:   string;
    price:         string;
    imageUrl:      string;
    averageRating: number;
    ratingCount:   number;
    score:         number;
    link:          string;
}
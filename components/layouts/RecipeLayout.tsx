import { FC, ReactNode } from "react";
import Head from "next/head";

import { NavbarUI } from "../ui";
import { RecipeWithIngredients } from "../../interfaces";

interface Props {
  children: ReactNode;
  recipe: RecipeWithIngredients;
}

export const RecipeLayout: FC<Props> = ({ children, recipe }) => {
  const { title, image } = recipe;
  return (
    <>
      <Head>
        <title>{`Recipe - ${title}`}</title>
        <meta name="author" content="dPanibra0" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content={`Kttita - ${title}`} />
        <meta property="og:description" content={`Pequeña decripción de la receta ${title}`} />
        <meta property="og:image" content={`${image}`} />
      </Head>
      <NavbarUI />
      <main style={{ padding: "0px 20px" }}>{children}</main>
    </>
  );
};

import { Grid, Card, Text, Spacer, Col } from "@nextui-org/react";
import { useRouter } from "next/router";
import { FC } from "react";
import { Recipe } from "../../interfaces";

interface Props {
  recipe: Recipe;
}

export const RecipeCard: FC<Props> = ({ recipe }) => {
  const router = useRouter();

  const onClick = () => {
    router.push(`/recipe/${recipe.id}`);
  };

  return (
    <Grid xs={6} sm={3} md={2} xl={1} key={recipe.id}>
      <Card isHoverable isPressable onClick={onClick}>
        <Card.Body css={{ p: 1 }}>
          <Card.Image
            src={recipe.image}
            objectFit="cover"
            width="100%"
            height={140}
            alt={recipe.title}
          />
        </Card.Body>
        <Card.Footer css={{ alignContent:'space-between', flexDirection:'column' }}>
          <Text transform="capitalize" b>
            {recipe.title}
          </Text>
          <Spacer css={{ flex: 1 }} />
          <Text
            css={{
              color: "$accents7",
              fontWeight: "$semibold",
              fontSize: "$sm",
            }}
          >
            #{recipe.id}
          </Text>
        </Card.Footer>
      </Card>
    </Grid>
  );
};

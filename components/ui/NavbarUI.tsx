import NextLink from 'next/link';
import { Navbar, Text } from "@nextui-org/react";

export const NavbarUI = () => {
  return (
    <Navbar shouldHideOnScroll variant="sticky">
      <NextLink href="/" passHref>
        <Navbar.Brand>
          <Text b color="inherit" hideIn="xs" css={{color:'$green600'}}>
            Kttita_Blog
          </Text>
        </Navbar.Brand>
      </NextLink>
      <Navbar.Content variant="underline" >
        <Navbar.Link isActive href="/">
          Home
        </Navbar.Link>
        <Navbar.Link
          href="/"
          isDisabled
          onClick={(e) => {
            e.preventDefault();
          }}
          css={{ cursor: "not-allowed" }}
        >
          Favoritos
        </Navbar.Link>
      </Navbar.Content>
    </Navbar>
  );
};

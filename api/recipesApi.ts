import axios from "axios";
import { apiVeganRecipeInfo } from "../interfaces";

const recipesApi = axios.create({
  baseURL: "https://api.spoonacular.com",
  params: { apiKey: "e7978d9ccfc645589126758d2af743da" },
});

export const getRecipeInfoById = async (id: String) => {
  return await (await recipesApi.get<apiVeganRecipeInfo>(`/recipes/${id}/information`)).data;
};

export default recipesApi;
